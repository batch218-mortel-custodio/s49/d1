// // mock database
// let posts = [];

// // posts ID
// let count = 1;

// use fetch here

fetch('https://jsonplaceholder.typicode.com/posts')
// Use the "json" method from the "Response" object and returns another "promise"
.then((response)=> response.json())
.then((data) => showPosts(data));




// ADD POST DATA
document.querySelector("#form-add-post").addEventListener("submit", (e) => {
	// //prevents default behavior of event thru submit
	e.preventDefault();


	fetch('https://jsonplaceholder.typicode.com/posts', {
		method: "POST", 
		// json stringify converts the object data into stringified JSON
		body: JSON.stringify({
			title: document.querySelector("#txt-title").value,
			body: document.querySelector("#txt-body").value,
			userId: 1
		}),

		// sets the header data of the "request" object to be sned to the backend
		headers: {
			"Content-Type" : "application/json; charset=UTF-8"
		}
	})
	.then((response)=> response.json())
	.then((data) => {
		console.log(data);
		alert(`Successfully added!`);

		document.querySelector("#txt-title").value = null;
		document.querySelector("#txt-body").value = null;
	});

	// posts.push({
	// 	id: count,
	// 	title: document.querySelector("#txt-title").value,
	// 	body: document.querySelector("#txt-body").value
	// });	

	// count++;

	// console.log(posts);
	// alert("Post successfully added in database!");

	//showPosts();

});

//RETRIEVE POST

const showPosts = (posts) => {
	let postEntries = "";

	posts.forEach((post) =>{
		postEntries += `
		<div id="post-${post.id}">
			<h3 id="post-title-${post.id}">${post.title}</h3>
			<p id="post-body-${post.id}">${post.body}</p>
			<button onClick="editPost(${post.id})">Edit</button>
			<button onClick="deletePost(${post.id})">Delete</button>
		</div>`
	});

	//console.log(postEntries);
	document.querySelector("#div-post-entries").innerHTML = postEntries;
};


// EDIT POST

const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector("#txt-edit-id").value = id;
	document.querySelector("#txt-edit-title").value = title;
	document.querySelector("#txt-edit-body").value = body;

	document.querySelector("#btn-submit-update").removeAttribute('disabled');
};


//////////////////////////////////////////////////////////////////




// UPDATE POST

document.querySelector("#form-edit-post").addEventListener("submit", (e) => {
	e.preventDefault();


	fetch('https://jsonplaceholder.typicode.com/posts/1', {
		method: "PUT", 
		// json stringify converts the object data into stringified JSON
		body: JSON.stringify({
			id: document.querySelector("#txt-edit-id").value,
			title: document.querySelector("#txt-edit-title").value,
			body: document.querySelector("#txt-edit-body").value,
			userId: 1
		}),

		// sets the header data of the "request" object to be sned to the backend
		headers: {
			"Content-Type" : "application/json; charset=UTF-8"
		}
	})
	.then((response)=> response.json())
	.then((data) => {
		console.log(data);
		alert(`Successfully added!`);

		document.querySelector("#txt-edit-id").value = null;
		document.querySelector("#txt-edit-title").value = null;
		document.querySelector("#txt-edit-body").value = null;

		document.querySelector("#btn-submit-update").setAttribute('disabled', true);
	});



/*	for (let i = 0; i < posts.length;  i++) {
		if(posts[i].id.toString() === document.querySelector("#txt-edit-id").value){
			posts[i].title = document.querySelector("#txt-edit-title").value;
			posts[i].body = document.querySelector("#txt-edit-body").value;

			showPosts(posts);
			alert("Successfully updated the post!");

			break;
		};
	};*/
});



//////////////////// DELETE POST ///////////////////////////

const deletePost = (id) => {



	fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
		method: "DELETE"
		// json stringify converts the object data into stringified JSON
	})
	.then((response) => {
		alert(`Successfully deleted!`);
		document.querySelector(`#post-${id}`).remove();
	});

	};



/*	console.log(id);

	let index = posts.findIndex(post =>{
		return post.id == id;
	});


	// ALTERNATIVE
	// const index = posts.map(post => post.id).indexOf(id);

	console.log(index);

	posts.splice(index, 1);

	showPosts();*/


